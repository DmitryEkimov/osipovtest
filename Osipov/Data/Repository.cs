﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Osipov.Models;

namespace Osipov.Data
{
    /// <summary>
    /// </summary>
    public class Repository
    {
        private static List<Task> tasks=null; 
        /// <summary>
        /// </summary>
        public Repository()
        {
            if (tasks==null)
            tasks = new List<Task>()
                        {
                            new Task() {num = 1, name = "Compute Mass", percentage = 20, state = true, user="client"},
                            new Task() {num = 2, name = "Make The word better", percentage = 99, state = false, user="member"}
                        };
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Task> Get(string username,bool isadmin)
        {
            return isadmin?tasks:tasks.Where(t=>t.user==username);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public Task Get(int num)
        {
            return tasks.FirstOrDefault(t => t.num == num);
        }

        /// <summary>
        /// </summary>
        /// <param name="task"></param>
        public int Add(Task task)
        {
            int num = tasks.Count==0? 1 : tasks.Max(t => t.num) + 1;
            task.num = num;
            tasks.Add(task);
            return num;
        }

        /// <summary>
        /// </summary>
        public void UpdatePercentage()
        {
            var array = tasks.ToArray();
            foreach(var task in array)
            {
                if(task.state)
                {
                    if(++task.percentage==100)
                    {
                        try
                        {
                            tasks.Remove(task);
                        }
                        finally
                        {

                        }
                    }
                }
            }
        }
    }
}
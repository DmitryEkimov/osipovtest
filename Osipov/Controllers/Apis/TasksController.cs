﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using Osipov.Data;
using Osipov.Models;

namespace Osipov.Controllers.Apis
{
    /// <summary>
    /// </summary>
    [Authorize]
    public class TasksController : ApiController
    {
        private Repository repository;

        /// <summary>
        /// </summary>
        public TasksController()
        {
            repository= new Repository();
        }
        /// <summary>
        /// </summary>
        public class PostNewTaskModel
        {
            /// <summary>
            /// </summary>
            public string Text { get; set; }
        }

        // GET api/tasks
        /// <summary>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Task> Get()
        {
           return repository.Get(User.Identity.Name, Roles.IsUserInRole("Administrators"));
        }

        // POST api/tasks
        /// <summary>
        /// </summary>
        /// <param name="newtask"></param>
        /// <returns></returns>
        public Task PostNewTask(PostNewTaskModel newtask)
        {
            var task = new Task() { num = 0, name = newtask.Text, percentage = 0, state = false, user = User.Identity.Name };
            task.num=repository.Add(task);
            return task;
        }
    }
}

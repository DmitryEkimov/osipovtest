﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Osipov.Data;

namespace Osipov.Controllers.Apis
{
    /// <summary>
    /// </summary>
    [Authorize]
    public class TaskActionController : ApiController
    {
        private Repository repository;

        /// <summary>
        /// </summary>
        public TaskActionController()
        {
            repository= new Repository();
        }
        /// <summary>
        /// </summary>
        public class PostActionTaskModel
        {
            public int Num { get; set; }
        }

        // PUT api/tasks/5
        /// <summary>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public bool Post(PostActionTaskModel data)
        {
            var task = repository.Get(data.Num);
            task.state = !task.state;
            return !task.state;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Osipov.Controllers
{
    /// <summary>
    /// </summary>
    [Authorize()]
    public class HomeController : Controller
    {
        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


    }
}

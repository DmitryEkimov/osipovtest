﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osipov.Models
{
    public class Task
    {
        public int num { get; set; }
        public string name { get; set; }
        public int percentage { get; set; }
        public bool state { get; set; }
        public string user { get; set; }
    }
}
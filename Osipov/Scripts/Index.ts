/// <reference path="jquery-1.8.d.ts"/>
/// <reference path="knockout.d.ts"/>
/// <reference path="knockoutmapping-2.0.d.ts"/>

// Module
module app.viewModels {

    // Class
    export class task {
        Num: number = 0;
        Name= ko.observable('');
        Percentage= ko.observable(0);
        State;
        Status :any;
        Actions :any;
        constructor (item) {
                this.Num = item.num;
                this.Name = ko.observable(item.name);
                this.Percentage = ko.observable(item.percentage);
                this.State = ko.observable(item.state);
                this.Status = ko.computed(()=> { return this.State() ? "Running..." : "Paused"; });
                this.Actions = ko.computed(()=> { return this.State() ? "Stop" : "Go"; });
        }
    }

    export class osipovViewModel {
        newTaskText = ko.observable('');
        tasks = ko.observableArray([]);

        addTask():void {
                $.ajax('/api/Tasks', {
                    data: ko.toJSON({ Text: this.newTaskText() }),
                    type: "post", contentType: "application/json",
                    success: (result)=> {
                        this.tasks.push(new task(result));
                        this.newTaskText("");
                    }
                });
                };
        updateTasks(): void {
                $.getJSON('/api/Tasks', (allData) => {
                    var mappedTasks = $.map(allData, (item) =>{ return new task(item); });
                    this.tasks(mappedTasks);
                });
                };
        ActionsTask(taskk:task):void {
                $.ajax('/api/TaskAction', {
                    data: ko.toJSON({ Num: taskk.Num }),
                    type: "post", contentType: "application/json",
                    success: function (result) {
                        taskk.State( !taskk.State());
                    }
                });
            };
    }
}

$(document).ready(() => {
    // Local variables
    var ViewModel = new app.viewModels.osipovViewModel;
    var timer = setInterval(()=>ViewModel.updateTasks(), 4000);
    ViewModel.updateTasks();
    ko.applyBindings(ViewModel);
    
});
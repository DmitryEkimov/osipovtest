var app;
(function (app) {
    (function (viewModels) {
        var task = (function () {
            function task(item) {
                var _this = this;
                this.Num = 0;
                this.Name = ko.observable('');
                this.Percentage = ko.observable(0);
                this.Num = item.num;
                this.Name = ko.observable(item.name);
                this.Percentage = ko.observable(item.percentage);
                this.State = ko.observable(item.state);
                this.Status = ko.computed(function () {
                    return _this.State() ? "Running..." : "Paused";
                });
                this.Actions = ko.computed(function () {
                    return _this.State() ? "Stop" : "Go";
                });
            }
            return task;
        })();
        viewModels.task = task;        
        var osipovViewModel = (function () {
            function osipovViewModel() {
                this.newTaskText = ko.observable('');
                this.tasks = ko.observableArray([]);
            }
            osipovViewModel.prototype.addTask = function () {
                var _this = this;
                $.ajax('/api/Tasks', {
                    data: ko.toJSON({
                        Text: this.newTaskText()
                    }),
                    type: "post",
                    contentType: "application/json",
                    success: function (result) {
                        _this.tasks.push(new task(result));
                        _this.newTaskText("");
                    }
                });
            };
            osipovViewModel.prototype.updateTasks = function () {
                var _this = this;
                $.getJSON('/api/Tasks', function (allData) {
                    var mappedTasks = $.map(allData, function (item) {
                        return new task(item);
                    });
                    _this.tasks(mappedTasks);
                });
            };
            osipovViewModel.prototype.ActionsTask = function (taskk) {
                $.ajax('/api/TaskAction', {
                    data: ko.toJSON({
                        Num: taskk.Num
                    }),
                    type: "post",
                    contentType: "application/json",
                    success: function (result) {
                        taskk.State(!taskk.State());
                    }
                });
            };
            return osipovViewModel;
        })();
        viewModels.osipovViewModel = osipovViewModel;        
    })(app.viewModels || (app.viewModels = {}));
    var viewModels = app.viewModels;

})(app || (app = {}));

$(document).ready(function () {
    var ViewModel = new app.viewModels.osipovViewModel();
    var timer = setInterval(function () {
        return ViewModel.updateTasks();
    }, 4000);
    ViewModel.updateTasks();
    ko.applyBindings(ViewModel);
});

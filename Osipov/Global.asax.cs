﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Osipov.Data;

namespace Osipov
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    /// <summary>
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        private static System.Timers.Timer timer = new System.Timers.Timer(4000);
        private static Repository repository;

        /// <summary>
        /// </summary>
        protected void Application_Start()
        {
            repository = new Repository();
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            timer.Enabled = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
        }
        static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            repository.UpdatePercentage();
        }

    }
}